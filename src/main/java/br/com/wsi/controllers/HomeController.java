package br.com.wsi.controllers;

import java.util.Arrays;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.wsi.daos.UsuarioDAO;
import br.com.wsi.models.Role;
import br.com.wsi.models.Usuario;

@Controller
public class HomeController {

	@Autowired
	private UsuarioDAO usuarioDao;
	
    @RequestMapping("/")
    public String index(){
    	return "home";
    }
    
    //Metodo que insere o primeiro usuario para inicio da utilizacao do sistema (nao utilizar isso em producao, por questoes de seguranca)
    @Transactional //Torna o metodo transacional, para fazer transacoes com o bd
    @ResponseBody //Retorna a String como String mesmo, e nao uma jsp
    @RequestMapping("/url-magica-maluca-isdhiahfihtiuhuihbnigniusewr")
    public String urlMagicaMaluca() {
    	Usuario usuario = new Usuario();
    	usuario.setNome("Admin");
    	usuario.setEmail("admin@wsi.com.br");
    	usuario.setSenha("$2a$04$qP517gz1KNVEJUTCkUQCY.JzEoXzHFjLAhPQjrg5iP6Z/UmWjvUhq"); //123456
    	usuario.setRoles(Arrays.asList(new Role("ROLE_ADMIN")));
    	
    	usuarioDao.gravar(usuario);
    	
    	return "Url Mágica executada";
    }
}
