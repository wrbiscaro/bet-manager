package br.com.wsi.conf;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

//Extende de uma classe que inicializa os filtros de seguranca do Spring Security
public class SpringSecurityFilterConfiguration extends AbstractSecurityWebApplicationInitializer {

}