package br.com.wsi.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.wsi.controllers.HomeController;
import br.com.wsi.daos.UsuarioDAO;
import br.com.wsi.models.Aposta;

//Classe de configuracao da Servlet
@EnableWebMvc //Habilita o Web MVC do Spring
@ComponentScan(basePackageClasses={HomeController.class, Aposta.class, UsuarioDAO.class}) //Indica onde o Spring poderá encontrar as classes, baseado em seus pacotes (uma classe por pacote)
public class AppWebConfiguration extends WebMvcConfigurerAdapter {
	
	//Método que ajuda o Spring a encontrar as views
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver(){
	    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	    resolver.setPrefix("/WEB-INF/views/"); //Pasta das views
	    resolver.setSuffix(".jsp"); //Extensao das views
	    return resolver;
	}
	
	//Habilita a servlet padrao do servidor para receber requisicoes de arquivos (css, js, imagens, etc.), deixando o Spring receber apenas as outras requisicoes
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}
