package br.com.wsi.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.wsi.daos.UsuarioDAO;

//Classe criada para as configuracoes das propriedades do Spring Security
@EnableWebMvcSecurity //Diz ao Spring que essa classe sera usada para configuracao de seguranca (o Spring ja faz uma pre-configuracao)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//Adiciona as paginas que serao protegidas por login. A ordem importa (primeiro fazer os bloqueios e depois liberar tudo). É necessário liberar tambem os arquivos css, javascript e imagens.
		http.authorizeRequests()
		//.antMatchers(HttpMethod.POST, "/produtos").hasRole("ADMIN")
		//.antMatchers(HttpMethod.GET, "/produtos").hasRole("ADMIN")
		//.antMatchers("/produtos/**").permitAll()
		.antMatchers(HttpMethod.GET, "/test").hasRole("ADMIN")
		.antMatchers("/resources/**").permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/url-magica-maluca-isdhiahfihtiuhuihbnigniusewr").permitAll()
		.anyRequest().authenticated()
		.and().formLogin().loginPage("/login").permitAll()
		.and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//Informa qual sera a classe responsavel pelos detalhes do usuario
		auth.userDetailsService(usuarioDao).passwordEncoder(new BCryptPasswordEncoder());
	}
}
