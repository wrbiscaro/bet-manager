package br.com.wsi.conf;

import javax.servlet.Filter;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//Extende a Servlet do Spring
public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {//Neste metodo as configuracoes sao iniciadas assim que a aplicacao é iniciada
		return new Class[] {SecurityConfiguration.class, AppWebConfiguration.class, JPAConfiguration.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"}; //Indica que o Spring atenderá todas as requisições a partir da raiz
	}

	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		
		//CharacterEncodingFilter: filtro de codificacao 
		//OpenEntityManagerInViewFilter: filtro que mantem o entity manager aberto ate a requisicao chegar na view, evitando o LazyInitializationException
		return new Filter[] {encodingFilter, new OpenEntityManagerInViewFilter()};
	}
	
	/*
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(RequestContextListener.class); //Diz para o Spring ouvir todos os contextos do sistema
		servletContext.setInitParameter("spring.profiles.active", "dev"); //Diz ao Spring que o perfil da aplicacao sera o de dev
	}
	*/
}
