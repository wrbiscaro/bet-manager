package br.com.wsi.models;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Aposta {

	@Id
	private int id;
	private String descricao;
	private Calendar data;
	private BigDecimal valor;
	private TipoAposta tipo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public TipoAposta getTipo() {
		return tipo;
	}
	public void setTipo(TipoAposta tipo) {
		this.tipo = tipo;
	}
}
