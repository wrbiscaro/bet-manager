# BetManager

Projeto de um gerenciador de apostas esportivas, criado utilizando alguns conceitos e tecnologias, como:

* Maven

* Integração contínua (com Jenkins)

* Métricas de qualidade do código-fonte (com Sonar)

* Testes automatizados e TDD (com JUnit)

* JSP

* JSTL

* Expression Language

* SpringMVC

* Hibernate/JPA

* Entre outras coisinhas mais...